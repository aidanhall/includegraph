#include "includegraph.h"

int main(int argc, char* argv[]) {
    /* Declarations. */
    int regex_flags = REG_NEWLINE;
    int32_t opt;
    int64_t max_depth = LONG_MAX;
    char* filename_regex_string = NULL;

    /* Arguments and options. */
    while ((opt = getopt(argc, argv, OPTSTRING)) != -1) {
	switch (opt) {
	    case 'r':
		if (filename_regex_string == NULL) {
		    fprintf(stderr,"Filename regex: %s\n", optarg);
		    filename_regex_string = strdup(optarg);
		} else {
		    fprintf(stderr, "Filename regex already specified.\n");
		}

		break;
	    case 'd':
		{ /* So endptr is only in scope for this case. */
		    char* endptr;
		    errno = 0;
		    max_depth = strtol(optarg, &endptr, 0);
		    if (errno != 0) {
			perror(optarg);
			exit(EXIT_FAILURE);
		    } else if (endptr == optarg) {
			fprintf(stderr, "No digits found: %s\n", optarg);
			exit(EXIT_FAILURE);
		    } else if (max_depth < 0) {
			fprintf(stderr, "Max depth must be positive or zero: %s\n", optarg);
			exit(EXIT_FAILURE);
		    }
		}
		break;
	    case 'I':
		fprintf(stderr, "Include directory: %s\n", optarg);
		break;
	    case 'c':
		fprintf(stderr, "Will attempt to prevent inclusion cycles.\n");
		break;
	    case 'h':
		printf(HELPSTRING,
			argv[0]);
		exit(EXIT_SUCCESS);
		break;
	    case 'E':
		fputs("Using extended regex.\n", stderr);
		regex_flags |= REG_EXTENDED;
		break;
	    default:
		fprintf(stderr, USAGESTRING, argv[0]);
		exit(EXIT_FAILURE);
	}
    }

    /* Get FILE* to root file. */
    FILE* root_file;

    if (optind >= argc) { 
	root_file = stdin; /* Read stdin if unspecified. */
    } else {
	if (strcmp(argv[optind], STDIN_MARKER) == 0) {
	    root_file = stdin; /* Read stdin if "-" specified. */
	} else {
	    /* TODO: Create a function to perform fopen using the include directories. */
	    root_file = fopen(argv[optind], "r"); /* Attempt to read filename. */
	    if (root_file == NULL) {
		perror(argv[optind]);
		exit(EXIT_FAILURE);
	    }
	}
    }

    if (root_file == stdin) {
	fprintf(stderr, "Reading from stdin.\n");
    } else {
	fprintf(stderr, "Reading from file.\n");
    }

    /* FILENAME REGEX */
    /* Checking and default values. */
    if (filename_regex_string == NULL)
	filename_regex_string = strdup(DEFAULT_FILENAME_REGEX);


    struct FilenameRegexes inclusions = {NULL, NULL};
    int status = FilenameRegexes_create(&inclusions, filename_regex_string, regex_flags);
    /* We only need to check the prefix,
     * since the create function will free and ->NULL one
     * if the other fails to be created.
     */
    if (status != 0) {
	fputs("Failed to create filename regexes.\n", stderr);
    } else {
	char **included_files = NULL;
	int n_included_files;
	find_included_files(root_file, inclusions, &included_files, &n_included_files);
	fprintf(stderr,"Include count: %d\n", n_included_files);
	for (int file_i = 0; file_i < n_included_files; ++file_i) {
	    printf("%s\n", included_files[file_i]);
	    free(included_files[file_i]);
	}
	free(included_files);

	/* Denouement. */
	FilenameRegexes_free(&inclusions);
    }
    free(filename_regex_string);
    fclose(root_file);
}

int find_included_files(FILE* base_file, struct FilenameRegexes match_incl, char ***included_files, int *n_included_files) {

    /* Don't dereference NULL pointers. */
    if (included_files == NULL || n_included_files == NULL || base_file == NULL)
	return 1;

    /* Set up included files list. */
    int n_allocated_strings = INITIAL_INCLUDED_FILES_ARRAY_LENGTH;
    *included_files = malloc(INITIAL_INCLUDED_FILES_ARRAY_LENGTH*sizeof(char*));
    *n_included_files = 0;




    char linebuffer[BUFFERSIZE];
    char *linebuffer_scanner;
    regmatch_t prefix_matches[MATCHES_LENGTH];

    while (fgets(linebuffer, BUFFERSIZE, base_file) != NULL) {
	/* Scans through linebuffer to find all matches on a line. */
	linebuffer_scanner = linebuffer;
	/* Match for prefix. */
	while (!regexec(match_incl.prefix, linebuffer_scanner, ARRAYLENGTH(prefix_matches), prefix_matches, 0)) {
	    fprintf(stderr, "Matches found on line: %s", linebuffer);
	    for (uint32_t match_i = 0; match_i < ARRAYLENGTH(prefix_matches) && prefix_matches[match_i].rm_so != -1; ++match_i) {
		fprintf(stderr, "Match from %d to %d: %.*s\n",
			prefix_matches[match_i].rm_so,
			prefix_matches[match_i].rm_eo,
			prefix_matches[match_i].rm_eo - prefix_matches[match_i].rm_so,
			linebuffer_scanner+prefix_matches[match_i].rm_so);

	    }
	    /* Match for suffix. */
	    regmatch_t suffix_matches[MATCHES_LENGTH];
	    const char* end_of_prefix = linebuffer_scanner + prefix_matches[0].rm_eo;
	    fprintf(stderr,"From end of prefix: %s", end_of_prefix);

	    if (!regexec(match_incl.suffix, end_of_prefix, ARRAYLENGTH(suffix_matches), suffix_matches, 0)) {
		fprintf(stderr, "Included file: %.*s\n",
			/* suffix_matches[0].rm_so,suffix_matches[0].rm_eo, */
			suffix_matches[0].rm_so,
			end_of_prefix);

		/* Add filename to list. */
		*n_included_files += 1;
		/* Reallocate to have extra space if necessary. */
		if (*n_included_files >= n_allocated_strings) {
		    n_allocated_strings *= RESIZE_FACTOR;
		    *included_files = realloc(*included_files, n_allocated_strings*sizeof(char*));
		}
		(*included_files)[*n_included_files-1] = strndup(end_of_prefix, suffix_matches[0].rm_so);

	    }

	    /* Move past end of matched suffix. */
	    linebuffer_scanner = (char*)end_of_prefix + suffix_matches[0].rm_eo;
	}
    }

    *included_files = realloc(*included_files, (*n_included_files)*sizeof(char*));
    return 0;
}

void FilenameRegexes_free(struct FilenameRegexes *regs) {
    regfree(regs->prefix);
    regfree(regs->suffix);
    free(regs->prefix);
    free(regs->suffix);
    regs->prefix = NULL;
    regs->suffix = NULL;
}

int FilenameRegexes_create(struct FilenameRegexes *regs, const char *filename_regex_string, const int regex_flags) {

    int return_status = 0;
    regs->prefix = malloc(sizeof(regex_t));
    regs->suffix = malloc(sizeof(regex_t));


    /* Free the other if one failed to malloc. */
    if (regs->prefix == NULL && regs->suffix != NULL) {
	free(regs->suffix);
	return_status = 1;
    } else if (regs->suffix == NULL && regs->prefix != NULL) {
	free(regs->prefix);
	return_status = 1;
    } else if (filename_regex_string == NULL) {
	FilenameRegexes_free(regs);
	return_status = 1;
    } else {

	char *filename_prefix_regex_string = NULL,
	     *filename_suffix_regex_string = NULL;
	{
	    char* marker_pos = strstr(filename_regex_string, FILENAME_MARKER);
	    if (marker_pos == NULL) {
		fprintf(stderr, "Filename regex must contain {-} to designate filename.\n");
		return_status = 1;
	    } else {
		filename_prefix_regex_string = strndup(filename_regex_string, marker_pos-filename_regex_string);
		if (filename_prefix_regex_string == NULL) {
		    perror("strndup");
		    return_status = 1;
		} else {
		    fprintf(stderr,"Filename prefix regex string: %s\n", filename_prefix_regex_string);
		    filename_suffix_regex_string = strdup(marker_pos+ARRAYLENGTH(FILENAME_MARKER)-1);
		}
	    }
	}

	if (filename_suffix_regex_string == NULL) {
	    free(filename_prefix_regex_string);
	    perror("strdup");
	    return_status = 1;
	} else {
	    fprintf(stderr,"Filename suffix regex string: %s\n", filename_suffix_regex_string);

	    /* Compile regexes. */
	    if ((return_status = regcomp(regs->prefix, filename_prefix_regex_string, regex_flags))) {
		regex_perror("Prefix", return_status, regs->prefix);

		free(regs->prefix);
	    } else if ((return_status = regcomp(regs->suffix, filename_suffix_regex_string, regex_flags))) {
		regex_perror("Suffix", return_status, regs->suffix);

		regfree(regs->prefix);
		free(regs->prefix);
		free(regs->suffix);
	    }
	    /* We don't need the strings any more. */
	    free(filename_prefix_regex_string);
	    free(filename_suffix_regex_string);
	}
    }

    return return_status;
}

void regex_perror(const char* message, int errcode, const regex_t *preg) {
    char errbuf[BUFFERSIZE];
    regerror(errcode, preg, errbuf, ARRAYLENGTH(errbuf));
    fprintf(stderr, "%s: %s\n", (message != NULL)? message : DEFAULT_REGEX_PERROR, errbuf);
}
