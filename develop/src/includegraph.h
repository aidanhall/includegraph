#pragma once

#include <regex.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <errno.h>

#define FILENAME_MARKER "{-}"
#define ANY_NUMBER_OF_NON_WHITESPACES_REGEX "\\S*"
#define DEFAULT_FILENAME_REGEX "^#\\s*include\\s*[\"<]" FILENAME_MARKER "[\">]"

#define OPTSTRING "r:d:I:chE"
#define STDIN_MARKER "-"

#define BUFFERSIZE (BUFSIZ)
#define ARRAYLENGTH(X) ((sizeof((X)))/sizeof((X)[0]))
#define MATCHES_LENGTH (1)

#define INITIAL_INCLUDED_FILES_ARRAY_LENGTH (4)
#define RESIZE_FACTOR (2)

#define FOR_RANGE(ITER, LENGTH) for (int ITER = 0; ITER < LENGTH; ++ITER)

#define USAGESTRING "Usage: %s [-c] [-E] [-r regex] [-d depth] [-I dirs] [file]\n"

#define OPT_SEP "\t"
#define HELPSTRING USAGESTRING\
    "includegraph - Recursively scan for and follow inclusion directives in a file.\n"\
    "Starts from [file]. Reads stdin if empty or " STDIN_MARKER ".\n"\
    "Optional arguments:\n"\
    "-c\t"	OPT_SEP "Keep a list of visited files and do not re-visit any "\
			"to prevent inclusion 'cycles'.\n"\
    "-E\t"	OPT_SEP "Use extended POSIX regex.\n"\
    "-r regex"  OPT_SEP "Use the specified regex instead of \"" DEFAULT_FILENAME_REGEX "\".\n"\
    "-d depth"  OPT_SEP "Follow up to [depth] levels of inclusion. No limit if unspecified.\n"\
    "-I dirs\t" OPT_SEP "Use [dirs] as 'include directories' to find files with relative path names\n"\
    "\t"	OPT_SEP "that aren't found relative to the current directory.\n"


/* Filename Regex structure */
struct FilenameRegexes {
    regex_t *prefix,
	    *suffix;
};

#define FILENAME_REGEXES_OK(X) ((X).prefix != NULL && (X).suffix != NULL)
/* Creates a filename regex structure. */
int FilenameRegexes_create(struct FilenameRegexes *regs, const char *filename_regex_string, const int regex_flags);
/* Frees a FilenameRegexes structure */
void FilenameRegexes_free(struct FilenameRegexes*);
#define DEFAULT_REGEX_PERROR "Regex"
/* Prints an error from a regex error code, with a preceding message like perror. */
void regex_perror(const char* message, int errcode, const regex_t *preg);
/* Stores a list of the included files in [filename] to [included_files] (malloc'ed), with its length in n_included_files. Returns 0 on success or 1 on error. */
int find_included_files(FILE* base_file, struct FilenameRegexes match_incl, char ***included_files, int *n_included_files);
