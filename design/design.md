# Include Graph
A program that creates a graph of the included files in a base file.
The output is a tree-type structure, created with Cairo.

## Features
* Command-line arguments: Single-character options only. Handled completely manually.
* Documentation: A man page.
* Custom include tracking with user-specified regular expressions.
* Output in as many Cairo formats as I can be bothered with. Minimum: PNG, SVG.
* The filename tracing and display should probably be separate programs:
    * JSON (with Jansson) transfer?

## Notes
* Inclusion tracing and graph creation should be separate modules so different output modules can be substituted.
* Includes are checked for with a regular expression that the user can specify, but defaults to one for C preprocessor includes.
* Since you will be using regex.h, fully embrace POSIX features.

## Anti-features.
* Written in strict C11.
* Consequently relies on POSIX standard features, since the C standard library lacks many basic features.
* Built with CMake because I'm more comfortable with it than Make now.
* Consider using Make instead to lean into the POSIX program aesthetic.

# Operation
## Vague Description:
1. Takes a single 'root' file as input.
2. Scans through the file and looks for includes: Commands indicating that another file should be copied into the current one.
3. Create a tree tracking all of the includes for a file.
4. Scan through recursively until all includes are tracked, or max depth is reached.
5. Keep a list of included files to avoid dependency cycles.
