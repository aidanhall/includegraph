# Prototype 1
This version will have:
* The regex include finding.
* Recursive scan.
* Appropriate command-line arguments and documentation:
    * -r [include regex]
	* This must have the pattern {-} representing where in the expression the filename should be.
	* The program matches for the text before the {-}, then for the text after,
	* then works out where the filename is based on the difference between where they start and end.
    * -d [max depth]
	* Read with strtol, base==0. Therefore, can use format strings as described in manual.
	* There is (effectively) no max depth if this is specified: It is set to LONG_MAX.
    * -I [include directories]
	* Include directories won't actually be added until at least prototype 2.
	* How it works: Store an array of char*'s to the include directory strings in argv.
	* A wrapper to `fopen` should be created that additionally searches for files in these directories.
	* Colon-delimited, allowing PATH variables like `CPATH` to be used.
    * -c : Prevent inclusion cycles if specified.
    * -h : Print help message and exit.
    * -E : Use extended POSIX regex. Default: do not.
    * Default argument: Root file. Reads from stdin if unspecified or name is "-".
    * Consider using `getopt` or `arg.h`:
	* `getopt`:
	    * POSIX. Powerful and flexible.
	    * Be careful to avoid using GNU extensions that change your code.
	    * Clearly much more complex on the backend.
	    * You'll be using regex.h anyway, so this doesn't hurt portability.
	* `arg.h`:
	    * Very simple and as portable as they get.
	    * Not 'clever': All arguments after first non-option argument are not handled.
* Textual output in the form: 'file X includes file Y on line Z'.
